package de.laura.fasthopper.mixin;

import net.minecraft.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(Entity.class)
public class EntityMixin {
    @ModifyConstant(method = "pushAwayFrom", constant = @Constant(doubleValue = 0.05000000074505806))
    public double pushAwayFrom(double old) {
        return 1.0;
    }
}
